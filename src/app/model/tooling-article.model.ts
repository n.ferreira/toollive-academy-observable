export interface ToolingArticle {
    name: string;
    price: number;
    picture?: string;
}