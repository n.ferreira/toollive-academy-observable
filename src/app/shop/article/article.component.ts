import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToolingArticle } from '../../model/tooling-article.model';

@Component({
  selector: 'article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  @Input() article: ToolingArticle | undefined;

  @Output() addBasket: EventEmitter<ToolingArticle> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  public addBackClick() {
    this.addBasket.emit(this.article);
  }

}
