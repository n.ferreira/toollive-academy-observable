import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ToolingArticle } from '../model/tooling-article.model';
import { ShopService } from '../services/shop.service';

@Component({
  selector: 'shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  public articles$: Observable<ToolingArticle[]> | undefined;

  constructor(private shopService: ShopService) { }

  ngOnInit(): void {
    this.articles$ = this.shopService.getItems();
  }

  public addToBasket(article: ToolingArticle) {
    this.shopService.addItem(article)
  }

}
