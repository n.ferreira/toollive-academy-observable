import { Component, OnInit } from '@angular/core';
import { ToolingArticle } from '../../model/tooling-article.model';
import { ShopService } from '../../services/shop.service';
import { Observable, tap } from 'rxjs';

@Component({
  selector: 'recap',
  templateUrl: './recap.component.html',
  styleUrls: ['./recap.component.scss']
})
export class RecapComponent implements OnInit {

  public articles: ToolingArticle[] = [];

  public articles$: Observable<ToolingArticle[]> = new Observable<ToolingArticle[]>()

  constructor(private shopService: ShopService) { }

  ngOnInit(): void {

    this.articles$ = this.shopService.paniers.pipe(
      // L'opérateur tap permet d'effectuer des effects de bord. Ici il est utilisé pour logger l'élément qui passe dans le pipe.
      tap((element => {
        console.log(element)
      }))
    )
  }

}
