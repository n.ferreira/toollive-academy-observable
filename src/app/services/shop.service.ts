import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, of } from 'rxjs';
import { ToolingArticle } from '../model/tooling-article.model';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  public paniers: BehaviorSubject<ToolingArticle[]> = new BehaviorSubject<ToolingArticle[]>([])

  constructor() { }

  public addItem(article: ToolingArticle) {

    // Le behaviorSubject garde en mémoire le dernier élément. Donc avec la fonction getValue() je viens récupérer la dernière valeur
    // du panier et j'y concatène le nouvel item afin d'avoir le panier remplis.
    const newPanier = [...this.paniers.getValue(), article];

    // Le .next() permet de transférer un élément à l'observable et de le diffuser à tous les éléments qui l'écoutent (les .subscribe()).
    this.paniers.next(newPanier);
  }

  public getItems(): Observable<ToolingArticle[]> {

    const articles: ToolingArticle[] = [
      {
        name: 'TRENT 700',
        price: 50000,
        picture: 'https://res.cloudinary.com/toollivecdn/image/authenticated/s--tykASP7x--/v1695821991/toollive/web/leasing/956A7644G03.webp'
      },
      {
        name: 'TRENT 200',
        price: 20000,
        picture: 'https://res.cloudinary.com/toollivecdn/image/authenticated/s--MgTMXO1l--/v1690969143/toollive/web/leasing/D71CRA00005G02.jpg'
      },
      {
        name: 'TRENT 3000',
        price: 90000,
        picture: 'https://res.cloudinary.com/toollivecdn/image/authenticated/s--iXblZnSe--/v1661939814/toollive/web/leasing/C71020-105.webp'
      },
      {
        name: 'Stand super chers',
        price: 300000,
        picture: 'https://res.cloudinary.com/toollivecdn/image/authenticated/s--AlydXZJq--/v1661932156/toollive/web/leasing/D71STA00004G08.webp'
      }
    ]

    return of(articles)

  }
}
