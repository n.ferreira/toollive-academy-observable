import { Component, OnInit } from '@angular/core';
import { ShopService } from '../../services/shop.service';
import { Observable, map } from 'rxjs';
import { ToolingArticle } from '../../model/tooling-article.model';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public articles$: Observable<number> = new Observable<number>()

  constructor(private shopService: ShopService) { }

  ngOnInit(): void {

    this.articles$ = this.shopService.paniers.pipe(
      map(articles => articles.length)
    )

  }

}
