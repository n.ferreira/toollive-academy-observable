import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription, filter, interval, map } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public text$: Observable<number> = new Observable()

  constructor() { }

  ngOnInit(): void {

    const obs$ = interval(1000);

    this.text$ = obs$.pipe(
      map(element => element * element)
    );
  }

}
